package pl.lombek.advent.y2021.day6.domain;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishTest {

    @Test
    void shouldItProperTimer() {
        // given
        Fish fish = new Fish(6);

        // when
        int timer = fish.getTimer();

        // then
        assertEquals(6, timer);
    }

    @Test
    void shouldItProperTimerWhenProduce() {
        // given
        Fish fish = Fish.produce();

        // when
        int timer = fish.getTimer();

        // then
        assertEquals(8, timer);
    }

    @Test
    void shouldItProperTimerWhenElapseDay() {
        // given
        Fish start = Fish.produce();

        // when
        Fish fish = start.plusDay();
        int timer = fish.getTimer();

        // then
        assertEquals(7, timer);
    }

    @Test
    void shouldItProperTimerWhenElapseDay2() {
        // given
        Fish start = new Fish(0);

        // when
        Fish fish = start.plusDay();
        int timer = fish.getTimer();

        // then
        assertEquals(6, timer);
    }

}
