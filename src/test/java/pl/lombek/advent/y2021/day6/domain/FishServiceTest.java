package pl.lombek.advent.y2021.day6.domain;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.lombek.advent.y2021.day6.FishParser;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishServiceTest {

    @ParameterizedTest
    @MethodSource("items")
    void testGenerate(List<Integer> previous, List<Integer> current) {
        // given
        FishService service = new FishService();
        FishHolder previousFishHolder = FishHolder.create(previous);

        // when
        FishHolder fishHolder = service.generate(previousFishHolder);
        List<Integer> timers = fishHolder.listOfTimer();

        // then
        assertEquals(current, timers);
    }

    private static Stream<Arguments> items() {
        return Stream.of(
                create("3,4,3,1,2", "2,3,2,0,1"),
                create("2,3,2,0,1", "1,2,1,6,0,8"),
                create("1,2,1,6,0,8", "0,1,0,5,6,7,8"),
                create("0,1,0,5,6,7,8", "6,0,6,4,5,6,7,8,8"),
                create("6,0,6,4,5,6,7,8,8", "5,6,5,3,4,5,6,7,7,8"),
                create("5,6,5,3,4,5,6,7,7,8", "4,5,4,2,3,4,5,6,6,7"),
                create("4,5,4,2,3,4,5,6,6,7", "3,4,3,1,2,3,4,5,5,6"),
                create("3,4,3,1,2,3,4,5,5,6", "2,3,2,0,1,2,3,4,4,5"),
                create("2,3,2,0,1,2,3,4,4,5", "1,2,1,6,0,1,2,3,3,4,8"),
                create("1,2,1,6,0,1,2,3,3,4,8", "0,1,0,5,6,0,1,2,2,3,7,8"),
                create("0,1,0,5,6,0,1,2,2,3,7,8", "6,0,6,4,5,6,0,1,1,2,6,7,8,8,8"),
                create("6,0,6,4,5,6,0,1,1,2,6,7,8,8,8", "5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8"),
                create("5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8", "4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8"),
                create("4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8", "3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8"),
                create("3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8", "2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7"),
                create("2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7", "1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8"),
                create("1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8", "0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8"),
                create("0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8", "6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8")
        );
    }

    private static Arguments create(String previous, String current) {
        return Arguments.of(
                new FishParser().parse(previous),
                new FishParser().parse(current)
        );
    }

    @Test
    void testGenerateAfter18Days() {
        // given
        FishService service = new FishService();
        List<Integer> initialState = Arrays.asList(3, 4, 3, 1, 2);
        int numberOfDays = 18;

        // when
        Map<Integer, FishHolder> map = service.generate(initialState, numberOfDays);
        int total = map.get(numberOfDays).size();

        // then
        assertEquals(26, total);
    }

    @Test
    void testGenerateAfter80Days() {
        // given
        FishService service = new FishService();
        List<Integer> initialState = Arrays.asList(3, 4, 3, 1, 2);
        int numberOfDays = 80;

        // when
        Map<Integer, FishHolder> map = service.generate(initialState, numberOfDays);
        int total = map.get(numberOfDays).size();

        // then
        assertEquals(5934, total);
    }

}