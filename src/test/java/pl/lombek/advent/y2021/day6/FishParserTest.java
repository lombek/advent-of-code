package pl.lombek.advent.y2021.day6;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishParserTest {

    @Test
    void parse1() {
        // given
        String input = "3,4,3,1,2";

        // when
        List<Integer> outcome = new FishParser().parse(input);

        // then
        assertEquals(Arrays.asList(3, 4, 3, 1, 2), outcome);
    }

    @Test
    void parse2() {
        // given
        String input = "0,2,6,3,4,4";

        // when
        List<Integer> outcome = new FishParser().parse(input);

        // then
        assertEquals(Arrays.asList(0, 2, 6, 3, 4, 4), outcome);
    }

}