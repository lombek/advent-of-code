package pl.lombek.advent.y2021.day6.domain;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class FishHolderTest {

    @Test
    void shouldItTheSameObjects() {
        // given
        FishHolder fishHolder = FishHolder.ofList(Arrays.asList(
                new Fish(3),
                new Fish(4),
                new Fish(3),
                new Fish(1),
                new Fish(2)
        ));

        // when
        FishHolder fishHolder2 = FishHolder.create(Arrays.asList(3, 4, 3, 1, 2));

        // then
        assertEquals(fishHolder, fishHolder2);
    }

    @Test
    void shouldItProperSize() {
        // given
        FishHolder fishHolder = FishHolder.create(Arrays.asList(3, 4, 3, 1, 2));

        // when
        int size = fishHolder.size();

        // then
        assertEquals(size, 5);
    }

    @Test
    void shouldItProperNumberOfFishToCreate() {
        // given
        FishHolder fishHolder = FishHolder.create(Arrays.asList(3, 4, 3, 1, 2));

        // when
        int size = fishHolder.numberOfFishToCreate();

        // then
        assertEquals(0, size);
    }

    @Test
    void shouldItProperNumberOfFishToCreate2() {
        // given
        FishHolder fishHolder = FishHolder.create(Arrays.asList(0, 1, 0, 5, 6, 7, 8));

        // when
        int numberOfFishToCreate = fishHolder.numberOfFishToCreate();

        // then
        assertEquals(2, numberOfFishToCreate);
    }

    @Test
    void shouldItProperNumberOfFishToCreate3() {
        // given
        FishHolder fishHolder = FishHolder.create(Arrays.asList(0, 1, 0, 5, 6, 0, 1, 2, 2, 3, 0, 1, 2, 2, 2, 3, 3, 4, 4, 5, 7, 8));

        // when
        int numberOfFishToCreate = fishHolder.numberOfFishToCreate();

        // then
        assertEquals(4, numberOfFishToCreate);
    }

}