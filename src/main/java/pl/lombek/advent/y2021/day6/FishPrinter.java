package pl.lombek.advent.y2021.day6;

import pl.lombek.advent.y2021.day6.domain.FishHolder;

import java.io.PrintStream;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class FishPrinter {

    private final PrintStream printStream;

    FishPrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    void print(Map<Integer, FishHolder> map) {
        String text = toRepresentativeValue(map);
        printStream.print(text);
    }

    private String toRepresentativeValue(
            Map<Integer, FishHolder> map
    ) {
        int size = map.size();
        int numberOfDays = size - 1;

        StringBuilder sb = new StringBuilder();

        for (int day : map.keySet()) {
            FishHolder fishHolder = map.get(day);
            if (day == 0) {
                sb.append("Initial state");
            } else {
                sb.append("After ");
            }

            sb.append(spaces(day, size));

            if (day > 0) {
                sb.append(day);
                if (day == 1) {
                    sb.append(" day ");
                } else {
                    sb.append(" days");
                }
            }

            sb.append(": ");
            sb.append(toRepresentativeValue(fishHolder));
            sb.append("\n");
        }

        FishHolder lastFishHolder = map.get(numberOfDays);
        int total = lastFishHolder.size();
        sb.append("Total of fish: ").append(total);

        return sb.toString();
    }

    private String toRepresentativeValue(FishHolder fishHolder) {
        return fishHolder
                .listOfTimer()
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(","));
    }

    private String spaces(int day, int size) {
        int c1 = String.valueOf(day).length();
        int c2 = String.valueOf(size).length();
        int spaces = c2 - c1;
        if (day == 0) {
            spaces--;
        }
        return IntStream.rangeClosed(1, spaces)
                .mapToObj(i -> new StringBuilder(" "))
                .reduce(new StringBuilder(), StringBuilder::append)
                .toString();
    }

}
