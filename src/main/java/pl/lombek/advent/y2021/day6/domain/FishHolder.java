package pl.lombek.advent.y2021.day6.domain;

import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FishHolder {

    private final List<Fish> fishes;

    private FishHolder(
            List<Fish> fishes
    ) {
        this.fishes = fishes;
    }

    int numberOfFishToCreate() {
        return (int) fishes
                .stream()
                .filter(Fish::isCreatingNew)
                .count();
    }

    void forEach(Consumer<? super Fish> action) {
        fishes.forEach(action);
    }

    public List<Integer> listOfTimer() {
        return fishes
                .stream()
                .map(Fish::getTimer)
                .collect(Collectors.toList());
    }

    public int size() {
        return fishes.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FishHolder that = (FishHolder) o;
        return Objects.equals(fishes, that.fishes);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fishes);
    }

    static FishHolder ofList(List<Fish> fishes) {
        return new FishHolder(fishes);
    }

    static FishHolder create(List<Integer> input) {
        return new FishHolder(
                input.stream()
                        .map(Fish::new).collect(Collectors.toList())
        );
    }

}
