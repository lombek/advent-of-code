package pl.lombek.advent.y2021.day6;

import pl.lombek.advent.y2021.day6.domain.FishHolder;
import pl.lombek.advent.y2021.day6.domain.FishService;

import java.util.Map;

// https://adventofcode.com/2021/day/6
public class Runner {

    public static void main(String[] args) {
        String input = "3,4,3,1,2";
        int numberOfDays = 18;

        FishParser parser = new FishParser();
        FishService service = new FishService();
        FishPrinter printer = new FishPrinter(System.out);

        Map<Integer, FishHolder> generated = service
                .generate(
                        parser.parse(input),
                        numberOfDays
                );
        printer.print(generated);
    }

}
