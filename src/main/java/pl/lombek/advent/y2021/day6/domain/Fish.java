package pl.lombek.advent.y2021.day6.domain;

import java.util.Objects;

public class Fish {

    private static final int RESET = 6;
    private static final int NEW = 8;

    private final int timer;

    Fish(int timer) {
        this.timer = timer;
    }

    int getTimer() {
        return timer;
    }

    boolean isCreatingNew() {
        return timer == 0;
    }

    Fish plusDay() {
        return new Fish(isCreatingNew() ? RESET : timer - 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fish fish = (Fish) o;
        return timer == fish.timer;
    }

    @Override
    public int hashCode() {
        return Objects.hash(timer);
    }

    static Fish produce() {
        return new Fish(NEW);
    }

}
