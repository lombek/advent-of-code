package pl.lombek.advent.y2021.day6;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FishParser {

    public List<Integer> parse(String input) {
        return Stream.of(input.split(","))
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }

}
