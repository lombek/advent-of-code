package pl.lombek.advent.y2021.day6.domain;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FishService {

    public Map<Integer, FishHolder> generate(List<Integer> input, int numberOfDays) {
        Map<Integer, FishHolder> map = new LinkedHashMap<>();

        FishHolder holderAtStart = FishHolder.create(input);
        map.put(0, holderAtStart);

        FishHolder previousHolder = holderAtStart;
        for (int day = 1; day <= numberOfDays; day++) {
            FishHolder currentHolder = generate(previousHolder);
            map.put(day, currentHolder);
            previousHolder = currentHolder;
        }

        return map;
    }

    FishHolder generate(FishHolder previousHolder) {
        List<Fish> outcome = new ArrayList<>();

        int numberOfFishToCreate = previousHolder.numberOfFishToCreate();

        previousHolder.forEach(fish -> outcome.add(fish.plusDay()));

        for (int i = 1; i <= numberOfFishToCreate; i++) {
            outcome.add(Fish.produce());
        }

        return FishHolder.ofList(outcome);
    }

}
